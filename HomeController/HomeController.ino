#include <LiquidCrystal.h>
#include "DHT.h"

String dataString = "";
String commandString = "";
String tempString = "";
String hmdtString = "";
boolean isConnected = false;
boolean stringComplete = false;

const int led1 = 46;
const int led2 = 48;
const int led3 = 50;

#define dhtPin 31
#define dhtType DHT11

LiquidCrystal lcd(41, 43, 45, 47, 49, 51);
DHT tempSensor(dhtPin, dhtType);

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  tempSensor.begin();
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  
}

void turnLedOnOff(int led, int state)
  {
    switch(led)
    {
      case 1:
        digitalWrite(led1, state);
        break;
      case 2:
        digitalWrite(led2, state);
        break;
      case 3:
        digitalWrite(led3, state);
        break;
      default:
        break;
    }
  }

void loop() {
  if(stringComplete)
  {
    commandString = dataString.substring(1,5);
    if(commandString.equals("OPEN"))
    {
      lcd.clear();
    }
    else if(commandString.equals("STOP"))
    {
      lcd.clear();
      lcd.print("Ready to connect");
    }
    else if(commandString.substring(0, 3).equals("LED"))
    {
      if (dataString.indexOf("ON") > 0) 
      {
        turnLedOnOff(atoi(dataString.substring(4).c_str()), 1);
      } 
      else
      {
        turnLedOnOff(atoi(dataString.substring(4).c_str()), 0);
      }
    }
    else if(commandString.equals("TEXT"))
    {
      String text = dataString.substring(5, dataString.length() - 1);

      lcd.clear();
      lcd.setCursor(0, 0);

      if(text.length() < 16)
      {
        lcd.print(text);
      }
      else
      {
        lcd.print(text.substring(0, 16));
        lcd.setCursor(0, 1);
        lcd.print(text.substring(16, 32));
      }
      
    }
    
    stringComplete = false;
    dataString = "";
  }

  float humidity = tempSensor.readHumidity();
  float temperature = tempSensor.readTemperature();

  tempString = "#TEMP" + String(temperature) + "\n";
  hmdtString = "#HMDT" + String(humidity) + "\n";

  Serial.write(tempString.c_str());
  Serial.write(hmdtString.c_str());

  delay(500);

}
  void serialEvent()
  {
    while(Serial.available())
    {
      char inChar = (char) Serial.read();
      dataString += inChar;
      if(inChar == '\n')
      {
        stringComplete = true;
      }
    }
  }
