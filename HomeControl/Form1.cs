﻿using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace HomeControl
{
    public partial class Form1 : Form
    {
        String[] ports = SerialPort.GetPortNames();
        Boolean isConnected = false;
        SerialPort port;
        public Form1()
        {
            InitializeComponent();
            Temperature.ReadOnly = true;
            Humidity.ReadOnly = true;
            AvailablePort.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach(String port in ports)
            {
                AvailablePort.Items.Add(port);
                if(ports[0] != null)
                {
                    AvailablePort.SelectedItem = ports[0];
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void connect()
        {
            String connectionPort = AvailablePort.GetItemText(AvailablePort.SelectedItem);
            if(!String.IsNullOrEmpty(connectionPort))
            {
                isConnected = true;
                port = new SerialPort(connectionPort, 9600, Parity.None, 8, StopBits.One);
                ReadSensorData.Enabled = true;
                
                port.Open();
                port.Write("#OPEN\n");
                ConnectButton.Text = "Disconnect";
            }
            
        }

        private void disconnect()
        {
            isConnected = false;
            port.Write("#STOP\n");
            Temperature.Text = "";
            Humidity.Text = "";
            ReadSensorData.Enabled = false;
            port.Close();
            ConnectButton.Text = "Connect";
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            if(isConnected)
            {
                disconnect();
            }
            else
            {
                connect();
            }
        }

        private void OnOrOff(CheckBox Led)
        {
            if(isConnected)
            {
                if(Led.Checked)
                {
                    port.Write("#LED" + Led.Name.Substring(3) + "ON\n");
                }
                else
                {
                    port.Write("#LED" + Led.Name.Substring(3) + "OFF\n");
                }
            }

        }

        private void Led1_CheckedChanged(object sender, EventArgs e)
        {
            OnOrOff(Led1);
        }

        private void Led2_CheckedChanged(object sender, EventArgs e)
        {
            OnOrOff(Led2);
        }

        private void Led3_CheckedChanged(object sender, EventArgs e)
        {
            OnOrOff(Led3);
        }

        private void WriteButton_Click(object sender, EventArgs e)
        {
            if(isConnected)
            {
                if(SevenSegDisplay.Text.Length < 33)
                {
                    port.Write("#TEXT" + SevenSegDisplay.Text + "\n");
                }
                else
                {
                    MessageBox.Show("Text is too long");
                }
            }
        }

        private void ReadSensorData_Tick(object sender, EventArgs e)
        {
            
                try
                {
                    String dataString = port.ReadLine();
                    String command = dataString.Substring(1, 4);
                    String value = dataString.Substring(5);
                    switch (command)
                    {
                        case "TEMP":
                        Temperature.Text = value;
                            break;
                        case "HMDT":
                        Humidity.Text = value;
                            break;
                        default:
                            break;
                    }
                }
                catch { }

            
        }
    }
}
