﻿namespace HomeControl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.Led1 = new System.Windows.Forms.CheckBox();
            this.Led2 = new System.Windows.Forms.CheckBox();
            this.Led3 = new System.Windows.Forms.CheckBox();
            this.SevenSegDisplay = new System.Windows.Forms.RichTextBox();
            this.WriteButton = new System.Windows.Forms.Button();
            this.AvailablePort = new System.Windows.Forms.ComboBox();
            this.Temperature = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Humidity = new System.Windows.Forms.TextBox();
            this.ReadSensorData = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(337, 37);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(106, 33);
            this.ConnectButton.TabIndex = 0;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // Led1
            // 
            this.Led1.AutoSize = true;
            this.Led1.Location = new System.Drawing.Point(28, 90);
            this.Led1.Name = "Led1";
            this.Led1.Size = new System.Drawing.Size(66, 21);
            this.Led1.TabIndex = 1;
            this.Led1.Text = "Led 1";
            this.Led1.UseVisualStyleBackColor = true;
            this.Led1.CheckedChanged += new System.EventHandler(this.Led1_CheckedChanged);
            // 
            // Led2
            // 
            this.Led2.AutoSize = true;
            this.Led2.Location = new System.Drawing.Point(116, 90);
            this.Led2.Name = "Led2";
            this.Led2.Size = new System.Drawing.Size(66, 21);
            this.Led2.TabIndex = 2;
            this.Led2.Text = "Led 2";
            this.Led2.UseVisualStyleBackColor = true;
            this.Led2.CheckedChanged += new System.EventHandler(this.Led2_CheckedChanged);
            // 
            // Led3
            // 
            this.Led3.AutoSize = true;
            this.Led3.Location = new System.Drawing.Point(199, 90);
            this.Led3.Name = "Led3";
            this.Led3.Size = new System.Drawing.Size(66, 21);
            this.Led3.TabIndex = 3;
            this.Led3.Text = "Led 3";
            this.Led3.UseVisualStyleBackColor = true;
            this.Led3.CheckedChanged += new System.EventHandler(this.Led3_CheckedChanged);
            // 
            // SevenSegDisplay
            // 
            this.SevenSegDisplay.Location = new System.Drawing.Point(28, 23);
            this.SevenSegDisplay.Name = "SevenSegDisplay";
            this.SevenSegDisplay.Size = new System.Drawing.Size(172, 47);
            this.SevenSegDisplay.TabIndex = 4;
            this.SevenSegDisplay.Text = "";
            // 
            // WriteButton
            // 
            this.WriteButton.Location = new System.Drawing.Point(218, 37);
            this.WriteButton.Name = "WriteButton";
            this.WriteButton.Size = new System.Drawing.Size(83, 33);
            this.WriteButton.TabIndex = 5;
            this.WriteButton.Text = "Write";
            this.WriteButton.UseVisualStyleBackColor = true;
            this.WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
            // 
            // AvailablePort
            // 
            this.AvailablePort.FormattingEnabled = true;
            this.AvailablePort.Location = new System.Drawing.Point(449, 37);
            this.AvailablePort.Name = "AvailablePort";
            this.AvailablePort.Size = new System.Drawing.Size(121, 24);
            this.AvailablePort.Sorted = true;
            this.AvailablePort.TabIndex = 6;
            // 
            // Temperature
            // 
            this.Temperature.Location = new System.Drawing.Point(116, 135);
            this.Temperature.Name = "Temperature";
            this.Temperature.Size = new System.Drawing.Size(122, 22);
            this.Temperature.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Temperature:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Humidity:";
            // 
            // Humidity
            // 
            this.Humidity.Location = new System.Drawing.Point(116, 194);
            this.Humidity.Name = "Humidity";
            this.Humidity.Size = new System.Drawing.Size(122, 22);
            this.Humidity.TabIndex = 10;
            // 
            // ReadSensorData
            // 
            this.ReadSensorData.Interval = 1000;
            this.ReadSensorData.Tick += new System.EventHandler(this.ReadSensorData_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 280);
            this.Controls.Add(this.Humidity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Temperature);
            this.Controls.Add(this.AvailablePort);
            this.Controls.Add(this.WriteButton);
            this.Controls.Add(this.SevenSegDisplay);
            this.Controls.Add(this.Led3);
            this.Controls.Add(this.Led2);
            this.Controls.Add(this.Led1);
            this.Controls.Add(this.ConnectButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.CheckBox Led1;
        private System.Windows.Forms.CheckBox Led2;
        private System.Windows.Forms.CheckBox Led3;
        private System.Windows.Forms.RichTextBox SevenSegDisplay;
        private System.Windows.Forms.Button WriteButton;
        private System.Windows.Forms.ComboBox AvailablePort;
        private System.Windows.Forms.TextBox Temperature;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Humidity;
        private System.Windows.Forms.Timer ReadSensorData;
    }
}

